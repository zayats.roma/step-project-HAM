// ====================
// OUR SERVICES SECTION

let servicesTabs = document.getElementsByClassName("services-menu")[0];
let servicesContentBlock = [...document.getElementsByClassName("services-contents")[0].children];
const activeServicesTab = servicesTabs.getElementsByClassName("services-active")[0];

for (let i = 0; i < servicesContentBlock.length; i++) {
    if (activeServicesTab.dataset.block === servicesContentBlock[i].dataset.block) {
        servicesContentBlock[i].hidden = false;
    } else {
        servicesContentBlock[i].hidden = true;
    }
}

servicesTabs.addEventListener("click", function (event) {
    servicesTabs.getElementsByClassName("services-active")[0].classList.remove("services-active");
    event.target.classList.add("services-active");

    const targetTab = event.target;
    for (let i = 0; i < servicesContentBlock.length; i++) {
        if (targetTab.dataset.block === servicesContentBlock[i].dataset.block) {
            servicesContentBlock[i].hidden = false;
        } else {
            servicesContentBlock[i].hidden = true;
        }
    }
});

// ================
// OUR WORK SECTION

const worksTabsMenu = document.getElementsByClassName("works-menu")[0];
const worksTabsArr = [...worksTabsMenu.children];
const worksContentsArr = [...document.getElementsByClassName("works-block")];
const activeWorksTab = worksTabsMenu.getElementsByClassName("works-active")[0];
const loadMoreWorksBtn = document.getElementsByClassName("works-loadmore-btn")[0];
const worksTabsBtnAll = worksTabsArr[4];

worksContentsArr.forEach(element => {
    if (element.dataset.category.includes(activeWorksTab.dataset.category)) {
        element.hidden = false;
    } else {
        element.hidden = true;
    }
})

worksTabsMenu.addEventListener("click", event => {
    worksTabsMenu.getElementsByClassName('works-active')[0].classList.remove("works-active");
    event.target.classList.add("works-active");

    for (let i = 0; i < worksContentsArr.length; i++) {
        if (worksContentsArr[i].dataset.category.includes(event.target.dataset.category)) {
            worksContentsArr[i].hidden = false;
        } else {
            worksContentsArr[i].hidden = true;
        }
    }
});


function shownElementCounter() {
    let unHiddenBlocksQuantity = 0;
    worksContentsArr.forEach(element => {
        if ([...element.attributes].length < 3) {
            unHiddenBlocksQuantity++;
        }
    })
    return unHiddenBlocksQuantity;
}

loadMoreWorksBtn.addEventListener("click", () => {
    if (shownElementCounter() < 12) {
        for (let i = 0; i < 12; i++) {
            if (worksContentsArr[i].hidden === true) {
                worksContentsArr[i].hidden = false;

                loadMoreWorksBtn.addEventListener("click", () => {
                    for (let i = 0; i < worksContentsArr.length; i++) {
                        if (worksContentsArr[i].hidden === true) {
                            worksContentsArr[i].hidden = false;

                            loadMoreWorksBtn.hidden = true;
                            worksTabsMenu.getElementsByClassName('works-active')[0].classList.remove("works-active");
                            worksTabsBtnAll.classList.add("works-active");
                        }
                    }
                });
            }
        }
    } else if (shownElementCounter() >= 12) {
        for (let i = 0; i < 24; i++) {
            if (worksContentsArr[i].hidden === true) {
                worksContentsArr[i].hidden = false;

                loadMoreWorksBtn.addEventListener("click", () => {
                    for (let i = 0; i < worksContentsArr.length; i++) {
                        if (worksContentsArr[i].hidden === true) {
                            worksContentsArr[i].hidden = false;

                            loadMoreWorksBtn.hidden = true;
                            worksTabsMenu.getElementsByClassName('works-active')[0].classList.remove("works-active");
                            worksTabsBtnAll.classList.add("works-active");
                        }
                    }
                });
            }
        }
    }

});

worksTabsArr.forEach(element => {
    element.addEventListener("click", () => {
        loadMoreWorksBtn.hidden = false;
    })
});

worksTabsBtnAll.addEventListener("click", () => {
    loadMoreWorksBtn.hidden = true;
});

// ================
// CAROUSEL SECTION

let galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: -90,
    slidesPerView: 4,
});

let galleryTop = new Swiper('.gallery-top', {
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
    loop:true,
    effect: 'flip',
});